#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools, AutoToolsBuildEnvironment

import os
from os import path

username = os.getenv("CONAN_USERNAME", "noface")
channel = os.getenv("CONAN_CHANNEL", "testing")
package_name = "usrsctp"
version      = "0.9.3+2f6478e"

class TestConanFile(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    #Conan dependencies
    requires = (
        "%s/%s@%s/%s" % (package_name, version, username, channel),
    )

    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/testing"
    )

    generators = "Waf"
    exports = "wscript"

    def imports(self):
        # Copy waf executable to project folder
        self.copy("waf", dst=".")

        self.copy("*.dll", dst="bin", src="bin")    # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.build_path = path.abspath("build")

        self.output.info("build path: " + self.build_path)
        self.output.info("conanfile_directory: " + self.conanfile_directory)
        self.output.info("current dir: " + path.abspath("."))

        self.run(
            "waf configure build -o %s" % (self.build_path),
            cwd=self.conanfile_directory)

    def test(self):
        exec_path = path.join(self.build_path, 'example')
        self.output.info("running test: " + exec_path)
        self.run(exec_path)
