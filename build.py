from conan.packager import ConanMultiPackager

import os

if __name__ == "__main__":
    builder = ConanMultiPackager(build_types=["Release"])
    builder.add_common_builds(shared_option_name="usrsctp:shared", pure_c=True)
    builder.run()
