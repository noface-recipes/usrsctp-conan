# usrsctp-conan

[Conan.io](https://www.conan.io/) package for [usrsctp](https://github.com/sctplab/usrsctp) library.

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[usrsctp repo](https://github.com/sctplab/usrsctp) for instructions about the library.


## About usrsctp


> **usrsctp** is a userland SCTP stack supporting FreeBSD, Linux, Mac OS X and Windows.

See the [library manual](https://github.com/sctplab/usrsctp/blob/master/Manual.md) for more info.

