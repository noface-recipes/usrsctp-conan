from conans import ConanFile, CMake
from conans.tools import os_info

import os
from os import path
import shutil

class UsrsctpConan(ConanFile):
    name        = "usrsctp"
    version     = "0.9.3+2f6478e"
    repo_url    = "https://github.com/sctplab/usrsctp"
    settings    = "os", "compiler", "arch"
    generators  = "cmake"
    exports     = ("CMakeLists.txt",)

    build_requires = (
        "cmake_installer/1.0@conan/stable"
    )

    options     = {
        "shared": [True, False]
    }
    default_options = (
        "shared=False",
        "cmake_installer:version=3.6.3"
    )

    commit = "2f6478e"
    git_dirname = name

    def source(self):
        # Clone the repository
        self.run("git clone %s %s" % (self.repo_url, self.git_dirname))
        self.run("git checkout %s" % self.commit, cwd=self.git_dirname)

        main_cmakelists = path.join(self.git_dirname, "CMakeLists.txt")
        shutil.move(main_cmakelists, path.join(self.git_dirname, "CMakeListsOriginal.cmake"))
        shutil.move("CMakeLists.txt", main_cmakelists)
        
    def build(self):
        #Make build dir
        self.build_dir = os.path.join(self.build_folder, "build")

        cmake = CMake(self)

        self.cmake_configure(cmake, self.build_dir)
        self.cmake_build_and_install(cmake, self.build_dir)

    def package_info(self):
        self.cpp_info.libs      = ["usrsctp"]
            

####################################### Helpers ################################################

    def cmake_configure(self, cmake, build_folder):
        cmake.configure(
            defs        = self.cmake_defs(),
            source_dir  = path.join(self.source_folder, self.git_dirname),
            build_dir   = build_folder)
        
    def cmake_build_and_install(self, cmake, build_dir):
        cmd = 'cmake --build . --target install %s' % (cmake.build_config)

        self.output.info("cwd: '%s' cmd: '%s'" % (build_dir, cmd))

        self.run(cmd, cwd=build_dir)
        
    def cmake_defs(self):
        """Generate definitions for cmake"""

        if not hasattr(self, 'package_folder'):
            self.package_folder = "dist"

        args = {
            'CMAKE_INSTALL_PREFIX' : self.package_folder
        }
        
        return args

    def try_make_dir(self, directory):
        try:
            os.mkdir(directory)
        except OSError:
            #dir already exist
            pass

        return dir
